from threading import Thread
from .logger import logger

from .console import SerialConsoleStream
from .tcpserver import TCPLogServer, TCPNetconsoleServer

import os
import serial.tools.list_ports
import threading
import traceback


class Machine:
    def __init__(self, salad, machine_id):
        self.salad = salad
        self.id = machine_id

        self.server = TCPLogServer(salad, machine_id, rw=True)
        self.server_for_logs = TCPLogServer(salad, machine_id, rw=False)

        self.__clients_lock = threading.RLock()
        self.__clients = list()

        self.console = None

    def stop(self):
        with self.__clients_lock:
            self.server.stop()
            self.server_for_logs.stop()

            for client in list(self.__clients):
                client.close()

            # Make sure our close() call removed the clients from the list of clients
            assert len(self.__clients) == 0

    @property
    def rw_client(self):
        with self.__clients_lock:
            for client in self.__clients:
                if client.rw:
                    return client
        return None

    @property
    def has_rw_client(self):
        return self.rw_client is not None

    def register_client(self, client):
        with self.__clients_lock:
            if client.rw and self.rw_client:
                raise ValueError("A client is already connected, re-try later!")

            self.__clients.append(client)

    def remove_client(self, client):
        with self.__clients_lock:
            try:
                self.__clients.remove(client)
            except ValueError:
                pass

    def send_to_machine(self, data):
        console = self.console
        if console is None or not console.is_valid or console.machine.id != self.id:
            self.console = console = self.salad.find_console_for_machine(self.id)

        if console:
            console.send(data)

    def send_to_clients(self, data):
        with self.__clients_lock:
            for client in self.__clients:
                client.send(data)


class Salad(Thread):
    def __init__(self):
        super().__init__(name='SaladThread')
        self._stop_event = threading.Event()

        self.netconsole_server = TCPNetconsoleServer(self, os.getenv("SALAD_TCPCONSOLE_PORT", 8100))

        # List of all the consoles
        self.__consoles_lock = threading.RLock()
        self.__consoles = dict()

        # Machines to Console association
        self.__machines_lock = threading.RLock()
        self.__machines = dict()

    def stop(self):
        # Signal for the main thread to stop polling for new devices
        self._stop_event.set()

        # Stop the netconsole TCP server
        self.netconsole_server.stop()

        # Stop all the Machine-related TCP servers
        with self.__machines_lock:
            for machine in self.__machines.values():
                machine.stop()
            self.__machines.clear()

        # Close all the consoles
        with self.__consoles_lock:
            for console in self.__consoles.values():
                console.stop()
            self.__consoles.clear()

        # Wait for the main thread to be over
        self.join()

    # Machines
    @property
    def machines(self):
        with self.__machines_lock:
            return list(self.__machines.values())

    def get_or_create_machine(self, machine_id):
        with self.__machines_lock:
            if machine := self.__machines.get(machine_id):
                return machine

            self.__machines[machine_id] = Machine(self, machine_id)
            return self.__machines[machine_id]

    # Consoles
    def add_console(self, console):
        with self.__consoles_lock:
            if console.stream_name in self.__consoles:
                raise ValueError("A console with the same name already exists")

            self.__consoles[console.stream_name] = console

    def find_console_for_machine(self, machine_id):
        with self.__consoles_lock:
            for console in self.__consoles.values():
                if console.machine_id == machine_id:
                    return console

    # Thread
    def run(self):
        while not self._stop_event.is_set():
            with self.__consoles_lock:
                # Make sure all our consoles are valid
                for port, console in dict(self.__consoles).items():
                    try:
                        if not console.is_valid:
                            console.stop()
                            del self.__consoles[port]
                    except Exception:
                        logger.error(traceback.format_exc())

                # Enumerate the consoles plugged
                try:
                    cur_ports = {p.device for p in serial.tools.list_ports.comports()}

                    # Remove all the ports that got disconnected
                    for port in set(self.__consoles.keys()) - cur_ports:
                        if not port.startswith("/dev/"):
                            continue

                        try:
                            logger.info(f"The serial device {port} got removed")
                            self.__consoles[port].close()
                            del self.__consoles[port]
                        except Exception:
                            logger.error(traceback.format_exc())

                    # Add any port that we do not have
                    for port in cur_ports - set(self.__consoles.keys()):
                        try:
                            console = SerialConsoleStream(self, port, port)
                            self.add_console(console)
                            logger.info(f"Found new serial device {port}")
                        except Exception:
                            logger.error(f"ERROR: Could not allocate a stream for the serial port {port}: {traceback.format_exc()}")
                except Exception:
                    logger.error(traceback.format_exc())

            # Wait up to a second
            self._stop_event.wait(timeout=1.0)


salad = Salad()
