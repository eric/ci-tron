============
Contributing
============

Process overview
================

Congratulations, you have made changes that you would like to get merge in the
``ci-tron`` project!

Here is a quick overview of the steps:

 * **Code of conduct**: Like all `Freedesktop.org <https://www.freedesktop.org>`_ project,
   contributing to this project requires accepting the terms of the
   `Contributor Covenant Code of Conduct <https://www.freedesktop.org/wiki/CodeOfConduct/>`_.
 * **Commit history**: Please split your changes into `atomic
   <https://docs.kernel.org/process/submitting-patches.html#separate-your-changes>`_
   commits, and write
   `good commit messages <https://who-t.blogspot.com/2009/12/on-commit-messages.html>`_
   motivating the change.
 * **Fork / merge request**: This project uses a fork/merge-request workflow:

   #. Fork `ci-tron <https://gitlab.freedesktop.org/gfx-ci/ci-tron/-/forks/new>`_;
   #. `Push <https://git-scm.com/docs/git-push#_examples>`_ your tree/commit to
      a branch in your fork;
   #. Open a merge request by following the `link GitLab provided during the push
      <https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-use-git-commands-locally>`_;
   #. Wait for the CI results, then address failures;
   #. Wait for review comments, then address them;
   #. Once the code is deemed ready by a trusted developer, it will be merged.

Frequently Asked Questions
==========================

I can't fork ci-tron, what is going on?
---------------------------------------

Due to an influx of spam, Freedesktop.org had to impose restrictions on new accounts.

Please see `this wiki page <https://gitlab.freedesktop.org/freedesktop/freedesktop/-/wikis/home>`_
for instructions on how to get full permissions.

The automated tests fail due to runner permission issues, what do I do?
-----------------------------------------------------------------------

Due to serious abuse of Freedesktop's testing infrastructure
(`details <https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/540>`_),
the use of the GitLab CI on freedesktop.org got restricted. Only group members
can trigger pipelines, everyone else needs the pipeline manually triggered by
an existing group member. See
`Freedesktop's wiki <https://gitlab.freedesktop.org/freedesktop/freedesktop/-/wikis/GitLab-CI#ci-infrastructure-restrictions>`_
for more details and possible remediation.

You may however use your CI-tron-based gateway to run your tests by exposing it
in your fork. This can be achieved by:

 * Obtaining a `runner registration project token <https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-project-runner-with-a-runner-authentication-token>`_;
 * Adding it to your ``/config/mars_db.yaml`` file (:ref:`syntax <MarsDB>`:);
 * Setting the following variables in your `project's CI variables <https://docs.gitlab.com/ee/ci/variables/#for-a-project>`_:

   * ``AARCH64_RUNNER_TAG_ARCH`` (default: ``aarch64``): Leave empty or ``cpu:arch:aarch64`` if you also have an amd64 runner
   * ``AARCH64_RUNNER_TAG_EXTRA`` (default: ''): ``CI-gateway``
   * ``AMD64_RUNNER_TAG_ARCH`` (default: ''): Leave empty or ``cpu:arch:x86_64`` if you also have an aarch64 runner
   * ``AMD64_RUNNER_TAG_EXTRA`` (default: ''): ``CI-gateway``
 * `Disabling the use of shared runners in the project <https://docs.gitlab.com/ee/ci/runners/runners_scope.html#disable-shared-runners-for-a-project>`_.
