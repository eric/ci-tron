#!/bin/sh

set -eux

if ! hash mcli; then
echo "mcli not found, and is required"
exit 1
fi

get_cached_path_to_artifact() {
    local url="$1"
    local cache_path="$2/cache/$(echo -n $1 | sha256sum | tr -d ' -')"

    mcli stat "$cache_path" > /dev/null 2> /dev/null || {
        local_file=$(mktemp)
        curl -Lo "$local_file" "$url" >&2
        mcli cp "$local_file" "$cache_path" >&2
        rm "$local_file"
    }

    echo $cache_path
}

# Wait for the minio service to start
for try in $(seq 1 20); do
    source {{ minio_env_file }} || true

    if [[ -v MINIO_ROOT_USER ]] && [[ -v MINIO_ROOT_PASSWORD ]]; then
        if mcli alias set minio http://localhost:{{ minio_port }} "$MINIO_ROOT_USER" "$MINIO_ROOT_PASSWORD"; then
            break
        fi
    fi

    if [ "${try}" -eq 20 ]; then
        echo "failed waiting for minio server";
        exit 1
    fi

    printf "minio server not responding, try %s of 20...\n" "$try"
    sleep 1
done


# Collect all the policies, users, groups, and buckets that are not wanted, then schedule them for deletion after
# we finish setting up the rest of the policies
#
# We really do not want to cleanup ressources when live-updating minio as it could potentially delete something
# needed by currently-running jobs. The only moment where it is safe to do that is at boot time.
#
# Detecting that we are booting is a little tricky, so instead we can ensure that we only perform the cleanup once per
# boot. This is done by creating a file in `/etc/minio` since it is kept between restarts of the service (unlike
# /tmp or /run), but lost between reboots (since it is part of the container).
#
cleanedup_path=/etc/minio/.cleanup-done
buckets_to_delete=
users_to_delete=
groups_to_delete=
policy_to_delete=
if ! [ -f "$cleanedup_path" ]; then
    buckets_to_delete=$(mcli ls --json minio | jq -r .key | grep -Ev '^({{ minio.buckets.keys() | map("regex_escape") | join("|") }})/$' || /bin/true)
    users_to_delete=$(mcli admin user ls --json minio | jq -r .accessKey | grep -Ev '^({{ minio.users.keys() | map("regex_escape") | join("|") }})$' || /bin/true)
    groups_to_delete=$(mcli admin group ls --json minio | jq -r .groups[] | grep -Ev '^({{ minio.groups.keys() | map("regex_escape") |join("|") }})$' || /bin/true)
    policy_to_delete=$(mcli admin policy ls minio | grep -Ev '^({{ minio.policies.keys() | map("regex_escape") | join("|") }})$' || /bin/true)

    touch "$cleanedup_path"
fi

# Set up the policies
{% for name, file_path in minio.policies.items() %}
mcli admin policy create minio "{{ name }}" "{{ file_path }}"
{% endfor %}


# Set up the users
declare -A groups
{% for name, cfg in minio.users.items() %}
if [ -n "{{ cfg.password }}" ]; then
    mcli admin user add minio "{{ name }}" "{{ cfg.password }}"

    # HACK: This command is not idempotent and will fail if the policy is already
    # attached... Let it fail, like they do upstream: https://github.com/minio/minio/issues/16897
    mcli admin policy attach minio "{{ cfg.policy }}" --user "{{ name }}" || /bin/true
{% for group in cfg.groups %}
    groups["{{ group }}"]="${groups['{{ group }}']:-} {{ name }}"
{% endfor %}
fi
{% endfor %}


# Set up the groups
{% for name, cfg in minio.groups.items() %}
if [ -n "${groups['{{ name }}']:-}" ]; then
    mcli admin group add minio "{{ name }}" ${groups['{{ name }}']}

    # HACK: This command is not idempotent and will fail if the policy is already
    # attached... Let it fail, like they do upstream: https://github.com/minio/minio/issues/16897
    mcli admin policy attach minio "{{ cfg.policy }}" --group "{{ name }}" || /bin/true
fi
{% endfor %}


# Reset the minio.env state
echo "MINIO_ROOT_USER=$MINIO_ROOT_USER" > {{ minio_env_file }}
echo "MINIO_ROOT_PASSWORD=$MINIO_ROOT_PASSWORD" >> {{ minio_env_file }}
echo "VALVETRACES_MINIO_PASSWORD=${VALVETRACES_MINIO_PASSWORD:-}" >> {{ minio_env_file }}
echo "VALVEFOSSILS_MINIO_PASSWORD=${VALVEFOSSILS_MINIO_PASSWORD:-}" >> {{ minio_env_file }}


{% for bucket, cfg in minio.buckets.items() %}

######### Setting up the {{ bucket }} bucket #########

    # Create the bucket, if missing
    bucket_mcli_path="minio/{{ bucket }}"
    mcli mb --ignore-existing $bucket_mcli_path

    # set bucket permission and validate that it was configured correctly
    # see: https://gitlab.freedesktop.org/gfx-ci/ci-tron/-/issues/125
    mcli anonymous set "{{ cfg.policy.anonymous }}" "$bucket_mcli_path"

    # grepping the permission from command output is not great, since mcli has
    # already changed output in the past, but until a better way is identified
    # this will have to do for now
    if ! mcli anonymous get "$bucket_mcli_path" | grep -q "{{ cfg.policy.anonymous }}"; then
        echo "ERROR: bucket '{{ bucket }}' does not appear to be '{{ cfg.policy.anonymous }}'!"
        exit 1
    fi

    # Populate the artifacts
    {% for artifact in cfg.populate | default([]) %}
        cached_path=$(get_cached_path_to_artifact "{{ artifact.url }}" "$bucket_mcli_path")
        {% for path in artifact.paths | default([]) %}
            mcli cp "$cached_path" "$bucket_mcli_path/{{ path['dst'] }}"
            {% for envvarname in path['envvars'] | default([]) %}
                echo "{{ envvarname }}=http://{{ hostname }}:{{ minio_port }}/{{ bucket }}/{{ path['dst'] }}" >> {{ minio_env_file }}
            {% endfor %}
        {% endfor %}
    {% endfor %}
{% endfor %}

######### Notify systemd that the we are done booting up #########

# NOTE: We have ensured that all the ressources we were asked to have are present,
# so we can tell systemd that we are done booting. After that, we may perform some
# cleanup operations on ressources that were leaked from previous jobs or projects.
systemd-notify --ready

######### Remove all ressources scheduled for deletion #########

if [ -n "$buckets_to_delete" ]; then
    for bucket in $buckets_to_delete; do
        mcli rb --force "minio/$bucket" || true
    done
fi

if [ -n "$users_to_delete" ]; then
    for user in $users_to_delete; do
        mcli admin user rm minio "$user" || true
    done
fi

if [ -n "$groups_to_delete" ]; then
    for group in $groups_to_delete; do
        mcli admin group rm minio "$group" || true
    done
fi

if [ -n "$policy_to_delete" ]; then
    for policy in $policy_to_delete; do
        mcli admin policy rm minio "$policy" || true
    done
fi
